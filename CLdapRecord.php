<?php
/**
 *
 *
 * @author: Christian Wittkowski <wittkowski@devroom.de>
 * @copyright: Copyright &copy; 2010-2011 Christian Wittkowski
 * @version: 0.4
 */

/**
 * CLdapRecord
 *
 * CLdapServer holds all the defined object classes.
 *
 * The used design pattern is Singleton. To get the one and
 * only instance of this class call CLdapServer::getInstance().
 *
 * @author Christian Wittkowski <wittkowski@devroom.de>
 * @version $Id: $
 * @package ext.ldaprecord
 * @since 0.4
 */
abstract class CLdapRecord extends CModel
{
    const BELONGS_TO = 'CLdapBelongsTo';
    const HAS_ONE    = 'CLdapHasOne';
    const HAS_MANY   = 'CLdapHasMany';

    private $_isNewRecord=false; // whether this node instance is new or not
    protected $_dn = null; // DN of this node
    private $_readDn = null; // set just one time after reading an entry from LDAP
    protected $_branch = '';
    protected $_branchDn = ''; // DN of the parent node
    protected $_filter = array(); // possible filter; used by reading
    protected $_dnAttributes = array(); // attributes used to create DN; order important!
    protected $_objectClasses = array(); // allowed object classes
    private $_md; // meta data
    protected $_attributes = array(); // array of actual attributes
    protected $_attributeTypes = array(); // array of attribute types
    /* TODO: use mandatory attributes to create rules. */
    protected $_mandatoryAttributes = array(); // array of manditory attributes
    protected $_related = array(); // attribute name => related objects
    protected $_overwrite = false; // overwrite existing attributes

    /**
     * Constructor.
     * @param string $scenario name. See {@link CModel::scenario} for more details about this parameter.
     */
    public function __construct($scenario = 'insert')
    {
        $this->setIsNewRecord(true);
        /* @var CLdapComponent $comp */
        $comp            = Yii::app()->getComponent('ldap');
        $this->_branchDn = $this->_branch . ',' . $comp->base_dn;
        $this->createAttributes();

        if ($scenario === null) {
            return;
        }

        $this->setScenario($scenario);

        $this->init();

        $this->attachBehaviors($this->behaviors());
    }

    /**
     * PHP getter magic method.
     * This method is overridden so that attributes can be accessed like properties.
     * @param string $name property name
     * @return mixed property value
     * @see getAttribute
     */
    public function __get($name)
    {

        $retval = $this->getAttribute($name);
        if ($retval === false) {
            if (isset($this->_related[$name])) {
                return $this->_related[$name];
            } else {
                if (isset($this->getMetaData()->relations[$name])) {
                    return $this->getRelated($name);
                } else {
                    return parent::__get('_'.$name);
                }
            }
        }
        return $retval;
    }

    /**
     * PHP setter magic method.
     * This method is overridden so that attributes can be accessed like properties.
     * @param string $name property name
     * @param mixed $value property value
     * @return mixed|void
     */
    public function __set($name, $value)
    {
        if (property_exists($this, $name)) {
            $this->$name = $value;
        } else {
            if ($this->hasAttribute($name)) {
                $this->setAttribute($name, $value);
            } else {
                if ($this->hasRelation($name)) {
                    $this->_related[$name] = $value;
                } else {
                    parent::__set($name, $value);
                }
            }
        }
    }


    /**
     * Returns the list of all attribute names of the model.
     * @return array list of attribute names.
     */
    public function attributeNames()
    {
        return array_keys($this->_attributes);
    }

    /**
     * Initializes this model.
     * This method is invoked when an instance is newly created and has
     * its {@link scenario} set.
     * You may override this method to provide code that is needed to initialize the model (e.g. setting
     * initial property values.)
     */
    public function init()
    {
    }

    /**
     * Returns the named attribute value.
     * If this record is the result of a query and the attribute is not loaded,
     * null will be returned.
     * You may also use $this->AttributeName to obtain the attribute value.
     * @param string $name the attribute name
     * @return mixed the attribute value. Null if the attribute is not set or does not exist.
     * @see hasAttribute
     */
    public function getAttribute($name)
    {
        if (property_exists($this, $name)) {
            return $this->$name;
        } else {
            if (isset($this->_attributes[$name])) {
                return $this->_attributes[$name];
            } else {
                if (isset($this->_attributes[strtolower($name)])) {
                    return $this->_attributes[strtolower($name)];
                } else {
                    if ('attributes' == $name) {
                        $returnValue = array();
                        foreach ($this->_attributes as $name => $info) {
                            $returnValue[$name] = $info;
                        }
                        return $returnValue;
                    }
                }
            }
        }
        return false;
    }

    /**
     * Sets the named attribute value.
     * You may also use $this->AttributeName to set the attribute value.
     * @param $name
     * @param $value
     * @param bool $overwrite
     * @throws CLdapException
     * @internal param \the $string attribute name
     * @internal param \the $mixed attribute value.
     * @return boolean whether the attribute exists and the assignment is conducted successfully
     * @see hasAttribute
     */
    public function setAttribute($name, $value, $overwrite = false)
    {
        if(($this->_overwrite && !$overwrite) || !$this->_overwrite && $overwrite){
            $overwrite = true;
        }

        if (isset($this->_attributes[$name]) || isset($this->_attributes[strtolower(
                $name
            )]) || '*' == $this->_objectClasses
        ) {
            if (isset($this->_attributes[strtolower($name)])) {
                $name = strtolower($name);
            }
            switch ($this->_attributeTypes[$name]) {
                case 'assoc':
                    preg_match($this->_attributeTypesData[$name], $value, $parts);
                    if (3 != count($parts)) {
                        throw new CLdapException(Yii::t(
                            'CLdapComponent.record',
                            'Parse value from attr \'{name}\' failed! (Wrong reg pattern)'
                        ), array('{name}' => $name));
                    }
                    $this->_attributes[$name][$parts[1]] = $parts[2];
                    break;
                case 'array':
                    if ($overwrite) {
                        $this->_attributes[$name] = array($value);
                    } else {
                        $this->_attributes[$name][] = $value;
                    }
                    break;
                default:
                    if (!empty($this->_attributes[$name]) && (!$overwrite)) {
                        $this->_attributeTypes[$name]  = 'array';
                        $firstvalue                        = $this->_attributes[$name];
                        $this->_attributes[$name] = array($firstvalue, $value);
                    } else {
                        $this->_attributes[$name] = $value;
                    }
                    break;
            }

        } else {
            if ('attributes' == $name && is_array($value)) {
                foreach ($value as $key => $val) {
                    $this->$key = $val;
                }
            } else {
                $aliases = CLdapSchema::getInstance()->getAttributeType($name)->getNames();
                $ok      = false;
                foreach ($aliases as $alias) {
                    if ($name == $alias) {
                        continue;
                    }
                    if (isset($this->_attributes[$alias]) || isset($this->_attributes[strtolower($alias)])) {
                        $ok = $this->setAttribute($alias, $value);
                        break;
                    }
                }
                if (!$ok) {
                    return parent::__set($name, $value);
                }
            }
        }
        return true;
    }

    /**
     * Checks if a property value is null.
     * This method overrides the parent implementation by checking
     * if the named attribute is null or not.
     * @param string $name the property name or the event name
     * @return boolean whether the property value is null
     */
    public function __isset($name)
    {
        if (isset($this->_attributes[$name])) {
            return true;
        } else {
            return parent::__isset($name);
        }
    }

    /**
     * Returns the related record(s).
     * This method will return the related record(s) of the current record.
     * If the relation is HAS_ONE or BELONGS_TO, it will return a single object
     * or null if the object does not exist.
     * If the relation is HAS_MANY, it will return an array of objects
     * or an empty array.
     * @param string $name the relation name (see {@link relations})
     * @param boolean $refresh whether to reload the related objects from database. Defaults to false.
     * @param array $params additional parameters that customize the query conditions as specified in the relation declaration.
     * @return mixed the related object(s).
     * @throws CLdapException if an error occurs.
     * @see hasRelated
     */
    public function getRelated($name, $refresh = false, $params = array())
    {
        if (!$refresh && $params === array() && (isset($this->_related[$name]) || array_key_exists(
                    $name,
                    $this->_related
                ))
        ) {
            return $this->_related[$name];
        }

        $md = $this->getMetaData();
        if (!isset($md->relations[$name])) {
            throw new CLdapException(Yii::t(
                'LdapRecord.record',
                '{class} does not have relation "{name}".',
                array('{class}' => get_class($this), '{name}' => $name)
            ));
        }
        //Yii::trace('lazy loading '.get_class($this).'.'.$name,'system.db.ar.CActiveRecord');
        $relation = $md->relations[$name];
        //echo '<pre>' . print_r($relation, true) . '</pre>';
        if ($this->getIsNewRecord() && ($relation instanceof CLdapHasOne || $relation instanceof CLdapHasMany)) {
            return $relation instanceof CLdapHasOne ? null : array();
        }
        if ($params !== array()) {
            $exists = isset($this->_related[$name]) || array_key_exists($name, $this->_related);
            if ($exists) {
                $save = $this->_related[$name];
            }
            unset($this->_related[$name]);
        }
        $this->_related[$name] = $relation->createRelationalRecord($this, $params);

        if (!isset($this->_related[$name])) {
            if ($relation instanceof CLdapHasMany) {
                $this->_related[$name] = array();
            }
            //			else if($relation instanceof CStatRelation)
            //				$this->_related[$name]=$relation->defaultValue;
            else {
                $this->_related[$name] = null;
            }
        }

        if ($params !== array()) {
            $results = $this->_related[$name];
            if ($exists) {
                $this->_related[$name] = $save;
            } else {
                unset($this->_related[$name]);
            }
            return $results;
        } else {
            return $this->_related[$name];
        }
    }

    /**
     * Returns a value indicating whether the named attribute is defined.
     * @param string $name the relation name
     * @return bool a value indicating whether the named attribute is defined.
     */
    public function hasAttribute($name)
    {
        $retval = isset($this->_attributes[$name]) || isset($this->_attributes[strtolower(
                $name
            )]) || '*' == $this->_objectClasses || $name === 'attributes';
        if (!$retval ) {

            $aliases = CLdapSchema::getInstance()->getAttributeType($name)->getNames();
            foreach ($aliases as $alias) {
                if ($name == $alias) {
                    continue;
                }
                if (isset($this->_attributes[$alias]) || isset($this->_attributes[strtolower($alias)])) {
                    $retval = true;
                    break;
                }
            }
        }
        return $retval;
    }

    public function delete(){
        if(!$this->getIsNewRecord()){
            Yii::trace(get_class($this).'.delete()','ldaprecord.CLdapRecord');
            if($this->beforeDelete()){
                $server = CLdapServer::getInstance();
                if($server->delete($this->createDn())){
                    $this->afterDelete();
                    return true;
                }
            }
        }
        return false;
    }
    /**
     * Returns a value indicating whether the named related object(s) is defined.
     * @param string $name the relation name
     * @return boolean a value indicating whether the named related object(s) is defined.
     */
    public function hasRelation($name)
    {
        return isset($this->getMetaData()->relations[$name]);
    }

    /**
     * Returns a value indicating whether the named related object(s) has been loaded.
     * @param string $name the relation name
     * @return boolean a value indicating whether the named related object(s) has been loaded.
     */
    public function hasRelated($name)
    {
        return isset($this->_related[$name]) || array_key_exists($name, $this->_related);
    }

    /**
     * Set the DN
     * @param $dn
     */
    public function setDn($dn)
    {
        $this->_dn = $dn;
    }

    /**
     * Return the Dn
     *
     * @return string with Dn.
     */
    public function getDn()
    {
        return $this->_dn;
    }

    /**
     * Gets the branchDN for the model
     * @return string
     */
    public function getBranchDn(){
        return $this->_branchDn;
    }
    /**
     * Sets the branchDn for the model.
     * @param string $dn the branchDn that this model is in.
     */
    public function setBranchDn($dn)
    {
        $this->_branchDn = $dn;
    }

    /**
     * Sets the overwrite mode for the model.
     * @param boolean $value
     */
    public function setOverwrite($value)
    {
        $this->_overwrite = $value;
    }

    /**
     * Return all the attributes.
     *
     * @param array $name
     * @return array with attributes and their type definition.
     */
    public function getAttributes($name=array())
    {
        return $this->_attributes;
    }

    /**
     * This method should be overridden to declare related objects.
     *
     * There are three types of relations that may exist between two active record objects:
     * <ul>
     * <li>BELONGS_TO: e.g. a member belongs to a team;</li>
     * <li>HAS_ONE: e.g. a member has at most one profile;</li>
     * <li>HAS_MANY: e.g. a team has many members;</li>
     * </ul>
     *
     * Each kind of related objects is defined in this method as an array with the following elements:
     * <pre>
     * 'varName'=>array('relationType', 'own_attribute', 'className', 'foreign_attribute', ...additional options)
     * </pre>
     * where 'varName' refers to the name of the variable/property that the related object(s) can
     * be accessed through; 'relationType' refers to the type of the relation, which can be one of the
     * following four constants: self::BELONGS_TO, self::HAS_ONE and self::HAS_MANY;
     * 'own_attribute' is the name of the attribute in the base object, if set to 'dn' 'foreign_attribute' can be set
     * to a php statement that returns the DN (see example below);
     * 'className' refers to the name of the ldap record class that the related object(s) is of;
     * and 'foreign_attribute' is the name of the attribute in the related object(s).
     *
     * Additional options may be specified as name-value pairs in the rest array elements:
     * <ul>
     * <li>'<attributename>': string, definition of an item for a possible filter</li>
     * </ul>
     *
     * Below is an example declaring related objects for 'Post' active record class:
     * <pre>
     * return array(
     *     'address'=>array(self::BELONGS_TO, 'addressUID', 'LdapAddress', 'uid'),
     *     'disks' => array(self::HAS_MANY, 'dn', 'LdapDisk', '$model->getDn()', array('sstDisk' => '*')),
     * );
     * </pre>
     *
     * @return array list of related object declarations. Defaults to empty array.
     */
    public function relations()
    {
        return array();
    }

    /**
     * Returns the static model of the specified Ldap class.
     *
     * @param string $className active record class name.
     * @return CLdapRecord ldap record model instance.
     */
    public static function model($className = __CLASS__)
    {
        /* @var CLdapRecord $model */
        $model      = new $className(null);
        $model->_md = new CLdapRecordMetaData($model);
        $model->attachBehaviors($model->behaviors());

        return $model;
    }

    /**
     * Returns the meta-data for this ldap record
     * @return CLdapRecordMetaData the meta for this ldap record class.
     */
    public function getMetaData()
    {
        if ($this->_md !== null) {
            return $this->_md;
        } else {
            return $this->_md = self::model(get_class($this))->_md;
        }
    }

    public function findSubTree($criteria)
    {
        if (!isset($criteria['branchDn'])) {
            $criteria['branchDn'] = $this->_branchDn;
        }
        $class = get_class($this);
        if ('LdapSubTree' != $class && !is_subclass_of($class, 'LdapSubTree')) {
            throw new CLdapException(Yii::t(
                    'CLdapComponent.record',
                    'findSubTree failed: used class is not type or subtype of \'LdapSubTree\'!'
                ),
                0x100002);
        }
        $server  = CLdapServer::getInstance();
        $entries = $server->findSubTree($this, $criteria);
        //echo '<pre>' . print_r($entries, true) . '</pre>';
        $branchDn = '';
        $nodes    = array();
        $retval   = array();
        for ($i = 0; $i < $entries['count']; $i++) {
            $objclasses = array();
            $item       = new $class();
            for ($j = 0; $j < $entries[$i]['count']; $j++) {
                if ('objectclass' == $entries[$i][$j]) {
                    $attr = $entries[$i][$j];
                    for ($k = 0; $k < $entries[$i][$attr]['count']; $k++) {
                        $objclasses[] = $entries[$i][$attr][$k];
                    }
                    continue;
                }
                $attr = $entries[$i][$j];
                for ($k = 0; $k < $entries[$i][$attr]['count']; $k++) {
                    $item->$attr = $entries[$i][$attr][$k];
                }
            }
            $item->_objectClasses = $objclasses;
            $item->_dn            = $entries[$i]['dn'];
            $item->_readDn        = $item->_dn;
            if (0 == $i) {
                $branchDn = $item->_dn;
            }
            $nodes[$item->_dn] = $item;
            if ($branchDn != $item->_dn) {
                $parentDn = substr($item->_dn, 1 + strpos($item->_dn, ','));
                if (isset($nodes[$parentDn])) {
                    if (!isset($nodes[$parentDn]->__children)) {
                        $nodes[$parentDn]->__children = array();
                    }
                    $nodes[$parentDn]->__children[] = $item;
                } else {
                    throw new CLdapException(Yii::t(
                        'CLdapComponent.record',
                        'findSubTree failed: parent \'{dn}\' not read!',
                        array('{dn}' => $parentDn)
                    ), 0x100001);
                }
            }
        }
        return $nodes[$branchDn];
    }


    public function findByAttributes($attributes)
    {
        $retval = $this->findAll($attributes);
        return 0 == count($retval) ? null : $retval[0];
    }

    public function findAll($criteria)
    {
        $class   = get_class($this);
        $server  = CLdapServer::getInstance();
        $criteria['branchDn']=$this->_branchDn;
        $criteria['attributes']=!empty($criteria['attributes'])?$criteria['attributes']:[];
        $entries = $server->findAll($this, $criteria);
        $items = array();
        for ($i = 0; $i < $entries['count']; $i++) {
            $item = new $class();
            for ($j = 0; $j < $entries[$i]['count']; $j++) {
                /* TODO: check if objectclasses are OK */
                if ('objectclass' == $entries[$i][$j]) {
                    continue;
                }
                $attr = $entries[$i][$j];
                for ($k = 0; $k < $entries[$i][$attr]['count']; $k++) {
                    $item->$attr = $entries[$i][$attr][$k];
                }
            }
            $item->_dn     = $entries[$i]['dn'];
            $item->_readDn = $item->_dn;
            $items[]      = $item;
        }
        if (isset($criteria['sort']) && '' != $criteria['sort']) {
            $sort  = explode('.', $criteria['sort']);
            $fname = 'compare' . ucfirst($sort[0]);
            if (isset($sort[1])) {
                $fname .= ucfirst($sort[1]);
            }
            $method = new ReflectionMethod($class, $fname);
            if ($method->isStatic()) {
                usort($items, array($class, $fname));
            }
        }
        return $items;
    }

    /**
     * Saves the current record.
     *
     * The record is inserted as a node if its {@link isNewEntry}
     * property is true (usually the case when the record is created using the 'new'
     * operator). Otherwise, it will be used to update the corresponding node
     * (usually the case if the record is obtained using one of those 'find' methods.)
     *
     * Validation will be performed before saving the record. If the validation fails,
     * the record will not be saved. You can call {@link getErrors()} to retrieve the
     * validation errors.
     *
     * If the record is saved via insertion, its {@link isNewEntry} property will be
     * set false, and its {@link scenario} property will be set to be 'update'.
     * And if its primary key is auto-incremental and is not set before insertion,
     * the primary key will be populated with the automatically generated key value.
     *
     * @param boolean $runValidation whether to perform validation before saving the record.
     * If the validation fails, the record will not be saved to database.
     * @param array $attributes list of attributes that need to be saved. Defaults to null,
     * meaning all attributes that are loaded from DB will be saved.
     * @return boolean whether the saving succeeds
     */
    public function save($runValidation = true, $attributes = null)
    {

        if (!$runValidation || $this->validate($attributes)) {
            return $this->getIsNewRecord() ? $this->insert($attributes) : $this->update($attributes);
        } else {
            return false;
        }
    }

    /**
     * Inserts a node based on this ldap record attributes.
     * Note, validation is not performed in this method. You may call {@link validate} to perform the validation.
     * After the record is inserted to Ldap successfully, its {@link isNewEntry} property will be set false,
     * and its {@link scenario} property will be set to be 'update'.
     * @param array $attributes list of attributes that need to be saved. Defaults to null,
     * meaning all attributes that are loaded from DB will be saved.
     * @return boolean whether the attributes are valid and the record is inserted successfully.
     * @throws CLdapException if the record is not new
     */
    protected function insert($attributes = null)
    {
        if (!$this->getIsNewRecord()) {
            throw new CLdapException(Yii::t(
                'LdapRecord.record',
                'The entry cannot be inserted to LDAP because it is not new.'
            ));
        }
        if($this->beforeSave()){
            $server = CLdapServer::getInstance();
            $dn = $this->createDn();
            if($server->add($dn, $this->createEntry(false))){
                $this->afterSave();
                $this->setReadDn($dn);
                $this->setIsNewRecord(false);
                return true;
            }
        }
        return false;
    }

    public function setReadDn($dn){
        $this->_readDn = $dn;
    }

    /**
     * Updates the node represented by this ldap record.
     * All loaded attributes will be saved.
     * Note, validation is not performed in this method. You may call {@link validate} to perform the validation.
     * @param array $attributes list of attributes that need to be saved. Defaults to null,
     * meaning all attributes that are loaded from DB will be saved.
     * @return boolean whether the update is successful
     * @throws CLdapException if the record is new
     */
    public function update($attributes = array())
    {
        if ($this->getIsNewRecord()) {
            throw new CLdapException(Yii::t(
                'LdapRecord.record',
                'The entry cannot be updated within LDAP because it is new.'
            ));
        }
        if($this->beforeSave()){
            if (!empty($attributes)) {
                $this->attributes = array_merge($this->_attributes, $attributes);

            }
            $entry = $this->createEntry(true);
            $dn = $this->createDn();
            $server = CLdapServer::getInstance();

            if ($this->_readDn == $dn) {
                $returnValue = $server->modify($dn, $entry );
            } else {
                $returnValue = $server->rename($this->_readDn, $dn);
                if ($returnValue) {
                    $returnValue = $server->modify($dn, $entry);
                }
            }
            $this->afterSave();

            return $returnValue;
        }
        return false;
     }

    public function findByDn($dn)
    {
        $item   = null;
        $server = CLdapServer::getInstance();
        $entry  = $server->findByDn($dn);
        if (1 < $entry['count']) {
            throw new CLdapException(Yii::t(
                'LdapRecord.record',
                'Wrong result count ({count}) on findByDn',
                array('{count}' => $entry['count'])
            ) );
        } else {
            if (1 == $entry['count']) {
                $item = $this->model(get_class($this));
                for ($j = 0; $j < $entry[0]['count']; $j++) {
                    /* TODO: check if objectclasses are OK */
                    if ('objectclass' == $entry[0][$j]) {
                        continue;
                    }
                    $attr = $entry[0][$j];
                    for ($k = 0; $k < $entry[0][$attr]['count']; $k++) {
                        $item->$attr = $entry[0][$attr][$k];
                    }
                }
                $item->_dn     = $entry[0]['dn'];
                $item->_readDn = $item->_dn;
                $item->setIsNewRecord(false);
            }
        }
        return $item;
    }
    /**
     * Returns if the current entry is new.
     * @return boolean whether the record is new and should be inserted when calling {@link save}.
     * This property is automatically set in constructor and {@link populateRecord}.
     * Defaults to false, but it will be set to true if the instance is created using
     * the new operator.
     */
    public function getIsNewRecord()
    {
        return $this->_isNewRecord;
    }

    /**
     * Sets if the entry is new.
     * @param boolean $value whether the record is new and should be inserted when calling {@link save}.
     * @see getIsNewRecord
     */
    public function setIsNewRecord($value)
    {
        $this->_isNewRecord=$value;
    }

    public function getFilter($name)
    {
        return $this->_filter[$name];
    }

    public function hasObjectClass($objClassName)
    {
        return in_array($objClassName, $this->_objectClasses);
    }

    public function removeAttributesByObjectClass($objClassName)
    {
        if ($this->hasObjectClass($objClassName)) {
            $schema   = CLdapSchema::getInstance();
            $objClass = $schema->getObjectClass($objClassName);
            if (null != $objClass) {
                foreach ($objClass->getAttributes() as $name => $info) {
                    unset($this->_attributes[$name]);
                }
                unset($this->_objectClasses[$objClassName]);
            }
        } else {
            throw new CLdapException(Yii::t(
                'LdapRecord.record',
                'Class "{class}" not found for removeAttributesByObjectClass',
                array('{class}', $objClassName)
            ));
        }
    }

    public function removeAttribute($names)
    {
        if (!is_array($names)) {
            $names = array($names);
        }
        foreach ($names as $name) {
            if (isset($this->_attributes[$name])) {
                unset($this->_attributes[$name]);
            } else {
                if (isset($this->_attributes[strtolower($name)])) {
                    unset($this->_attributes[strtolower($name)]);
                }
            }
        }
    }

    public function createDn()
    {
        $dn = '';
        foreach ($this->_dnAttributes as $name) {
            if ('' != $dn) {
                $dn .= ',';
            }
            $dn .= $name . '=' . $this->$name;
        }
        $this->_dn = $dn . ',' . $this->_branchDn;
        return $this->_dn;
    }


    private function createEntry($isModify)
    {
        $entry = array();
        foreach ($this->_attributes as $key => $value) {
            if ('dn' != $key && !empty($value)) {
                if (is_array($value)) {
                    foreach ($value as $val) {
                        $entry[$key][] = $val;
                    }
                } else {
                    $entry[$key] = $value;
                }
            }
        }
        if (!$isModify) {
            if (0 != count($this->_objectClasses)) {
                $entry['objectclass'] = array();
                foreach ($this->_objectClasses as $class) {
                    $entry['objectclass'][] = $class;
                }
            } else {
                throw new CLdapException(Yii::t(
                    'LdapRecord.record',
                    'Failed to createEntry for ldap_add. No objectClass(es) defined!'
                ));
            }
        }
        return $entry;
    }

    // TODO: change name to createAttributeDefinitions
    protected function createAttributes()
    {
        $schema = CLdapSchema::getInstance();
        //$this->_attributes['dn'] = null;
        if ('*' == $this->_objectClasses) {
            return;
        }
        foreach ($this->_objectClasses as $objClassName) {
            $objClass = $schema->getObjectClass($objClassName);
            if (null != $objClass) {
                if (is_null($this->_attributes)) {
                    $this->_attributes = $objClass->getAttributes();
                    $this->_attributeTypes = $objClass->getAttributeTypes();
                    $this->_mandatoryAttributes = $objClass->getMandatoryAttributes();
                } else {
                    $this->_attributes = array_merge($this->_attributes, $objClass->getAttributes());
                    $this->_attributeTypes = array_merge($this->_attributeTypes, $objClass->getAttributeTypes());
                    $this->_mandatoryAttributes = array_merge($this->_mandatoryAttributes, $objClass->getMandatoryAttributes());
                }
                // TODO: is labeledURIObject also used in other LDAP servers than OpenLdap
            }
            if ('labeledURIObject' == $objClassName) {
                $this->_attributes['member'] = array('mandatory' => false, 'type' => 'array');
            }
        }
        //echo '<pre>' . print_r($this->_attributes, true) . '</pre>';
    }



    /**
     * This event is raised before the record is saved.
     * By setting {@link CModelEvent::isValid} to be false, the normal {@link save()} process will be stopped.
     * @param CModelEvent $event the event parameter
     */
    public function onBeforeSave($event)
    {
        $this->raiseEvent('onBeforeSave',$event);
    }

    /**
     * This event is raised after the record is saved.
     * @param CEvent $event the event parameter
     */
    public function onAfterSave($event)
    {
        $this->raiseEvent('onAfterSave',$event);
    }

    /**
     * This event is raised before the record is deleted.
     * By setting {@link CModelEvent::isValid} to be false, the normal {@link delete()} process will be stopped.
     * @param CModelEvent $event the event parameter
     */
    public function onBeforeDelete($event)
    {
        $this->raiseEvent('onBeforeDelete',$event);
    }

    /**
     * This event is raised after the record is deleted.
     * @param CEvent $event the event parameter
     */
    public function onAfterDelete($event)
    {
        $this->raiseEvent('onAfterDelete',$event);
    }

    /**
     * This event is raised before an AR finder performs a find call.
     * This can be either a call to CActiveRecords find methods or a find call
     * when model is loaded in relational context via lazy or eager loading.
     * If you want to access or modify the query criteria used for the
     * find call, you can use {@link getDbCriteria()} to customize it based on your needs.
     * When modifying criteria in beforeFind you have to make sure you are using the right
     * table alias which is different on normal find and relational call.
     * You can use {@link getTableAlias()} to get the alias used for the upcoming find call.
     * Please note that modification of criteria is fully supported as of version 1.1.13.
     * Earlier versions had some problems with relational context and applying changes correctly.
     * @param CModelEvent $event the event parameter
     * @see beforeFind
     */
    public function onBeforeFind($event)
    {
        $this->raiseEvent('onBeforeFind',$event);
    }

    /**
     * This event is raised after the record is instantiated by a find method.
     * @param CEvent $event the event parameter
     */
    public function onAfterFind($event)
    {
        $this->raiseEvent('onAfterFind',$event);
    }
    /**
     * This method is invoked before saving a record (after validation, if any).
     * The default implementation raises the {@link onBeforeSave} event.
     * You may override this method to do any preparation work for record saving.
     * Use {@link isNewRecord} to determine whether the saving is
     * for inserting or updating record.
     * Make sure you call the parent implementation so that the event is raised properly.
     * @return boolean whether the saving should be executed. Defaults to true.
     */
    protected function beforeSave()
    {
        if($this->hasEventHandler('onBeforeSave'))
        {
            $event=new CModelEvent($this);
            $this->onBeforeSave($event);
            return $event->isValid;
        }
        else
            return true;
    }

    /**
     * This method is invoked after saving a record successfully.
     * The default implementation raises the {@link onAfterSave} event.
     * You may override this method to do postprocessing after record saving.
     * Make sure you call the parent implementation so that the event is raised properly.
     */
    protected function afterSave()
    {
        if($this->hasEventHandler('onAfterSave'))
            $this->onAfterSave(new CEvent($this));
    }

    /**
     * This method is invoked before deleting a record.
     * The default implementation raises the {@link onBeforeDelete} event.
     * You may override this method to do any preparation work for record deletion.
     * Make sure you call the parent implementation so that the event is raised properly.
     * @return boolean whether the record should be deleted. Defaults to true.
     */
    protected function beforeDelete()
    {
        if($this->hasEventHandler('onBeforeDelete'))
        {
            $event=new CModelEvent($this);
            $this->onBeforeDelete($event);
            return $event->isValid;
        }
        else
            return true;
    }

    /**
     * This method is invoked after deleting a record.
     * The default implementation raises the {@link onAfterDelete} event.
     * You may override this method to do postprocessing after the record is deleted.
     * Make sure you call the parent implementation so that the event is raised properly.
     */
    protected function afterDelete()
    {
        if($this->hasEventHandler('onAfterDelete'))
            $this->onAfterDelete(new CEvent($this));
    }


    /**
     * This method is invoked before an AR finder executes a find call.
     * The find calls include {@link find}, {@link findAll}, {@link findByPk},
     * {@link findAllByPk}, {@link findByAttributes}, {@link findAllByAttributes},
     * {@link findBySql} and {@link findAllBySql}.
     * The default implementation raises the {@link onBeforeFind} event.
     * If you override this method, make sure you call the parent implementation
     * so that the event is raised properly.
     * For details on modifying query criteria see {@link onBeforeFind} event.
     */



    /**
     * This method is invoked after each record is instantiated by a find method.
     * The default implementation raises the {@link onAfterFind} event.
     * You may override this method to do postprocessing after each newly found record is instantiated.
     * Make sure you call the parent implementation so that the event is raised properly.
     */
    protected function afterFind()
    {
        if($this->hasEventHandler('onAfterFind'))
            $this->onAfterFind(new CEvent($this));
    }

}










