<?php
/**
 * CLdapServer class file.
 *
 * @author: Christian Wittkowski <wittkowski@devroom.de>
 * @copyright: Copyright &copy; 2010-2011 Christian Wittkowski
 * @version: 0.4
 */

/*
 * TODO: Check communication with ldaps server
 * TODO: implement caching mechanism for objectclasses and attributetypes
 */

/**
 * CLdapServer
 *
 * CLdapServer holds all the defined object classes.
 *
 * The used design pattern is Singleton. To get the one and
 * only instance of this class call CLdapServer::getInstance().
 *
 * @author Christian Wittkowski <wittkowski@devroom.de>
 * @version $Id: $
 * @package ext.ldaprecord
 * @since 0.4
 */
final class CLdapServer
{
    /**
     * @var CLdapServer static .
     */
    private static $_instance = null;

    /**
     * @var array with configuration params.
     */
    private $_config = null;
    /**
     *  mixed|resource $_connection ldap link identifier
     */
    private $_connection = null;
    /**
     * @var
     */
    private $_anonymous = false;

    /**
     * Constructor private
     *
     * establish connection to Ldap server
     */
    private function __construct()
    {
        $this->_config = array();
        /* @var CLdapComponent $comp */
        $comp                     = Yii::app()->getComponent('ldap');
        $this->_config['server']  = $comp->server;
        $this->_config['port']    = $comp->port;
        $this->_config['base_dn'] = $comp->base_dn;

        if (isset($comp->bind_rdn)) {
            $this->_config['bind_rdn'] = $comp->bind_rdn;
        }
        if (isset($comp->bind_pwd)) {
            $this->_config['bind_pwd'] = $comp->bind_pwd;
        }
        if ($this->_config != null) {
            $this->connect();
            if (isset($comp->bind_rdn) && isset($comp->bind_pwd)) {
                $ldapbind = @ldap_bind($this->_connection, $this->_config['bind_rdn'], $this->_config['bind_pwd']);
            } else {
                $this->_anonymous = true;
                $ldapbind         = @ldap_bind($this->_connection);
            }
            if ($ldapbind === false) {
                throw new CLdapException(
                    Yii::t(
                        'CLdapComponent.server',
                        'ldap_bind failed ({errno}): {message}',
                        array(
                            '{errno}'   => ldap_errno($this->_connection),
                            '{message}' => ldap_error($this->_connection)
                        )
                    ), ldap_errno($this->_connection));
            }
        }
    }

    /**
     * Get all defined objectclasses and attributetypes from LDAP server.
     *
     * @return array [objectclasses]=>CLdapObjectClass, [attributetypes]=>CLdapAttributeType
     * @throws CLdapException if the LDAP server generates an error.
     */
    public function getDefinitions()
    {
        $result = $this->read($this->_config['base_dn'], 'objectClass=*', array('subschemaSubentry'));

        $entries           = $this->getEntries($result);
        $entry             = $entries[0];
        $subschemasubentry = $entry[$entry[0]][0];

        $result = $this->read($subschemasubentry, 'objectClass=*', array('objectclasses', 'attributetypes'));

        $entries = $this->getEntries($result);

        return $this->buildDefinitions($entries);
    }

    /**
     * Return the base Dn from configuration.
     *
     * @return string with base Dn.
     */
    public function getBaseDn()
    {
        return $this->_config['base_dn'];
    }

    /**
     * Return if connection is anonymous.
     *
     * @return boolean is anonymous.
     */
    public function isAnonymous()
    {
        return $this->_anonymous;
    }

    /**
     * Finds all ldap records satisfying the specified condition (one level only.).
     *
     * @param CLdapRecord $model
     * @param array $criteria
     * @return array a complete result information in a multi-dimensional array on success and false on error.
     * @throws CLdapException if the LDAP server generates an error.
     */
    public function findAll($model, $criteria = array('attributes' => null))
    {

        $filter = '';
        if (isset($criteria['attributes'])) {
            if (!is_null($criteria['attributes'])) {
                $filter = '(&';
                foreach ($criteria['attributes'] as $key => $value) {
                    if ('' != $value) {
                        if ('*' == $value) {
                            $filter .= "($key=*)";
                        } else {
                            $filter .= "($key=$value)";
                        }
                    }
                }
                $filter .= ')';
                if ('(&)' == $filter) {
                    $filter = $model->getFilter('all');
                }
            }
        } else {
            if (isset($criteria['filter'])) {
                $filter = $criteria['filter'];
            } else {
                throw new CLdapException(Yii::t(
                    'CLdapComponent.server',
                    'findAll: neither attributes nor filter set in criteria!'
                ));
            }
        }
        if (strpos($criteria['branchDn'], $this->_config['base_dn']) === false) {
            $branchDn = $criteria['branchDn'] . ',' . $this->_config['base_dn'];
        } else {
            $branchDn = $criteria['branchDn'];
        }
        $result  = $this->ldapList($branchDn, $filter);
        $entries = $this->getEntries($result);

        return $entries;
    }

    /**
     * Finds all ldap records satisfying the specified condition (subtree.).
     *
     * @param CLdapRecord $model
     * @param array $criteria
     * @return array a complete result information in a multi-dimensional array on success and false on error.
     * @throws CLdapException if the LDAP server generates an error.
     */
    public function findSubTree($model, $criteria)
    {
        $filter = '(&';
        if (isset($criteria['attr'])) {
            foreach ($criteria['attr'] as $key => $value) {
                if ('' != $value) {
                    if ('*' == $value) {
                        $filter .= "($key=*)";
                    } else {
                        $filter .= "($key=*$value*)";
                    }
                }
            }
        }
        $filter .= ')';
        if ('(&)' == $filter) {
            $filter = $model->getFilter('all');
        }
        //echo "findAll: $filter<br/>";
        if (strpos($criteria['branchDn'], $this->_config['base_dn']) === false) {
            $branchDn = $criteria['branchDn'] . ',' . $this->_config['base_dn'];
        } else {
            $branchDn = $criteria['branchDn'];
        }
        $result  = $this->search($branchDn, $filter);
        $entries = $this->getEntries($result);

        return $entries;
    }

    /**
     * Find one ldap record satisfying the Dn.
     *
     * @param string $dn
     * @return array a complete result information in a multi-dimensional array on success and false on error.
     * @throws CLdapException if the LDAP server generates an error.
     */
    public function findByDn($dn)
    {
        if (strpos($dn, $this->_config['base_dn']) === false) {
            $dn = trim($dn,',') . ',' . $this->_config['base_dn'];
        }
        $result = null;
        try {

            $result = $this->read($dn, '(objectclass=*)');
        }catch(Exception $ex){
            return null;
        }
        return $this->getEntries($result);


    }

    /**
     * Implements ldap_read function
     * @param $dn
     * @param $filter
     * @param array $attributes
     * @param int $attributesOnly
     * @param int $sizeLimit
     * @param int $timeLimit
     * @param int $deRef
     * @return resource
     * @throws CLdapException
     */
    private function read(
        $dn,
        $filter,
        $attributes = [],
        $attributesOnly = 0,
        $sizeLimit = 0,
        $timeLimit = 10,
        $deRef = LDAP_DEREF_NEVER
    ) {
        $return = @ldap_read(
            $this->_connection,
            $dn,
            $filter,
            $attributes,
            $attributesOnly,
            $sizeLimit,
            $timeLimit,
            $deRef
        );
        if ($return === false) {
            throw new CLdapException(Yii::t(
                'CLdapComponent.server',
                'ldap_read failed ({errno}): {message}',
                array('{errno}' => ldap_errno($this->_connection), '{message}' => ldap_error($this->_connection))
            ), ldap_errno($this->_connection));
        }
        return $return;
    }

    /**
     * Modify an existing leaf with defined Dn.
     *
     * @param string $dn DN to be modified
     * @param array $entry all attributes as key->value
     * @return boolean success.
     * @throws CLdapException if the LDAP server generates an error.
     */
    public function modify($dn, $entry)
    {
        //echo "dn: $dn<br/><pre>" . print_r($entry, true) . '</pre>';
        $retval = @ldap_modify($this->_connection, $dn, $entry);
        if (!$retval) {
            throw new CLdapException(Yii::t(
                'CLdapComponent.server',
                'ldap_modify failed ({errno}): {message}',
                array('{errno}' => ldap_errno($this->_connection), '{message}' => ldap_error($this->_connection))
            ), ldap_errno($this->_connection));
        }

        return true;
    }

    /**
     * Add a new leaf with defined Dn.
     *
     * @param string $dn dn to be added
     * @param array $entry all attributes as key->value
     * @return boolean success.
     * @throws CLdapException if the LDAP server generates an error.
     */
    public function add($dn, $entry)
    {
        echo "dn: $dn<br/><pre>" . print_r($entry, true) . '</pre>';
        if (strpos($dn, $this->_config['base_dn']) === false) {
            $dn = trim($dn,',') . ',' . $this->_config['base_dn'];
        }

        $retval = @ldap_add($this->_connection, $dn, $entry);
        if ($retval === false) {
            throw new CLdapException(Yii::t(
                'CLdapComponent.server',
                'ldap_add failed ({errno}): {message}',
                array('{errno}' => ldap_errno($this->_connection), '{message}' => ldap_error($this->_connection))
            ), ldap_errno($this->_connection));
        }

        return true;
    }

    /**
     * Delete entry from ldap server
     *
     * @param string $dn DN to be deleted can be partial dn
     * @return bool return true if delete succeeds
     * @throws CLdapException
     */
    public function delete($dn){

        if (strpos($dn, $this->_config['base_dn']) === false) {
            $dn = trim($dn,',') . ',' . $this->_config['base_dn'];
        }
        $result = @ldap_delete($this->_connection,$dn);
        if($result === false){
            throw new CLdapException(Yii::t(
                    'CLdapComponent.server',
                    'ldap_delete failed ({errno}): {message}',
                    array('{errno}' => ldap_errno($this->_connection), '{message}' => ldap_error($this->_connection))
                ), ldap_errno($this->_connection));

        }
        return true;
    }

    /**
     * Change Dn.
     * @param string $dn the old Dn.
     * @param string $newDn the new Dn.
     * @param null $newParent
     * @param bool $deleteOldRdn
     * @throws CLdapException
     * @return boolean success.
     */
    public function rename($dn, $newDn, $newParent = null, $deleteOldRdn = false)
    {
        //$newDn .= ',' . $this->_config['base_dn'];
        //echo "dn: $dn $newDn</pre>";
        $retval = @ldap_rename($this->_connection, $dn, $newDn, $newParent, $deleteOldRdn);
        if (!$retval) {
            throw new CLdapException(Yii::t(
                'CLdapComponent.server',
                'ldap_rename failed ({errno}): {message}',
                array('{errno}' => ldap_errno($this->_connection), '{message}' => ldap_error($this->_connection))
            ), ldap_errno($this->_connection));
        }

        return true;
    }

    /**
     * Close ldap connection
     */
    public function close()
    {
        if (!is_null($this->_connection)) {
            ldap_unbind($this->_connection);
        }
    }

    /**
     * Static method which returns the singleton instance of this class.
     *
     * @return CLdapServer
     */
    public static function getInstance()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new CLdapServer();
        }
        return self::$_instance;
    }

    /**
     * Is there an instance of this class?
     *
     * @return boolean whether the instance was created or not
     */
    public static function hasInstance()
    {
        return !is_null(self::$_instance);
    }

    /**
     * Don't allow cloning of this class from outside
     */
    private function __clone()
    {
    }

    /**
     * @param $entries
     * @return array
     */
    private function buildDefinitions($entries)
    {
        $objectclasses  = array();
        $attributetypes = array();
        for ($i = 0; $i < $entries[0]['count']; $i++) {
            if ('objectclasses' == $entries[0][$i]) {
                $objclasses = $entries[0][$entries[0][$i]];
                for ($j = 0; $j < $objclasses['count']; $j++) {
                    $objclass = new CLdapObjectClass($objclasses[$j]);
                    foreach ($objclass->getNames() as $name) {
                        $objectclasses[$name] = $objclass;
                    }
                }
            } else {
                if ('attributetypes' == $entries[0][$i]) {
                    $attrtypes = $entries[0][$entries[0][$i]];
                    for ($j = 0; $j < $attrtypes['count']; $j++) {
                        $attrtype = new CLdapAttributeType($attrtypes[$j]);
                        foreach ($attrtype->getNames() as $name) {
                            $attributetypes[$name] = $attrtype;
                        }
                    }
                }
            }
        }
        return array('objectclasses' => $objectclasses, 'attributetypes' => $attributetypes);
    }

    /**
     * @param array $result
     * @return array
     * @throws CLdapException
     */
    public function getEntries($result)
    {
        $entries = @ldap_get_entries($this->_connection, $result);
        if ($entries === false) {
            throw new CLdapException(Yii::t(
                'CLdapComponent.server',
                'ldap_get_entries failed ({errno}): {message}',
                array('{errno}' => ldap_errno($this->_connection), '{message}' => ldap_error($this->_connection))
            ), ldap_errno($this->_connection));
        }
        return $entries;
    }
    /**
     * @param array $result
     * @return array
     * @throws CLdapException
     */
    private function getFirstEntry($result)
    {
        $entries = @ldap_first_entry($this->_connection, $result);
        if ($entries === false) {
            throw new CLdapException(Yii::t(
                'CLdapComponent.server',
                'ldap_get_entries failed ({errno}): {message}',
                array('{errno}' => ldap_errno($this->_connection), '{message}' => ldap_error($this->_connection))
            ), ldap_errno($this->_connection));
        }
        return $entries;
    }

    private function connect()
    {
        $this->_connection = @ldap_connect(
                $this->_config['server'],
                $this->_config['port']
            ) or die('LDAP connect failed!');
        if ($this->_connection === false) {
            throw new CLdapException(Yii::t(
                'CLdapComponent.server',
                'ldap_connect to {server} failed',
                array('{server}' => $this->_config['server'])
            ));
        }
        ldap_set_option($this->_connection, LDAP_OPT_PROTOCOL_VERSION, 2);
        if(!@ldap_start_tls($this->_connection)){
            sleep(5);
            ldap_close($this->_connection);
            $this->connect();
        }
    }

    /**
     * @param $dn
     * @param $filter
     * @param array $attributes
     * @param int $attributesOnly
     * @param int $sizeLimit
     * @param int $timeLimit
     * @param int $dereference
     * @throws CLdapException
     * @internal param $branchDn
     * @return resource
     */
    private function search($dn, $filter,$attributes = array(),$attributesOnly=0,$sizeLimit=0,$timeLimit=10,$dereference=LDAP_DEREF_NEVER)
    {
        $result = @ldap_search($this->_connection, $dn, $filter,$attributes,$attributesOnly,$sizeLimit,$timeLimit,$dereference);
        if ($result === false) {
            throw new CLdapException(Yii::t(
                'CLdapComponent.server',
                'ldap_search failed ({errno}): {message}',
                array('{errno}' => ldap_errno($this->_connection), '{message}' => ldap_error($this->_connection))
            ), ldap_errno($this->_connection));
        }
        return $result;
    }

    /**
     * @param string $dn
     * @param string $filter
     * @param array $attributes
     * @param int $attributesOnly
     * @param int $sizeLimit
     * @param int $timeLimit
     * @param int $dereference
     * @throws CLdapException
     * @internal param string $branchDn
     * @return resource
     */
    private function ldapList($dn, $filter,$attributes = array(),$attributesOnly=0,$sizeLimit=0,$timeLimit=10,$dereference=LDAP_DEREF_NEVER)
    {
        $result = @ldap_list($this->_connection, $dn, $filter,$attributes,$attributesOnly,$sizeLimit,$timeLimit,$dereference);
        if ($result === false) {
            throw new CLdapException(Yii::t(
                'CLdapComponent.server',
                'ldap_list failed ({errno}): {message}',
                array('{errno}' => ldap_errno($this->_connection), '{message}' => ldap_error($this->_connection))
            ), ldap_errno($this->_connection));
        }
        return $result;
    }
}
