<?php

/**
 * Class Group
 * @property array $_filter
 * @property array $_objectClasses
 * @property $uid
 *
 * @return CLdapRecord
 */
class Group extends CLdapRecord
{
    protected $_branch = 'ou=Group';
    protected $_branchDn = '';
    protected $_dnAttributes = array('cn');
    protected $_filter = array('all' => 'objectClass=*'); // defined filter(s)
    protected $_objectClasses = array('posixGroup'); // allow all object classes
    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Group CLdapRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function getNextGid(){
        $systemAccounts = SystemAccounts::model()->findByDn(Yii::app()->getComponent('ldap')->nextGidAccount);
        $systemAccounts->uid++;
        $systemAccounts->save();
        return $systemAccounts->uid;
    }
    protected function beforeSave(){
        if($this->getIsNewRecord()) {
            $this->gidNumber = $this->getNextGid();

        }
        return parent::beforeSave();
    }
}