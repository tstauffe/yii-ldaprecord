<?php

/**
 * Class Person
 * @property array $_filter
 * @property array $_objectClasses
 * @property $uid
 *
 * @return CLdapRecord
 */
class Person extends CLdapRecord
{
    protected $_branch = 'ou=People';
    protected $_branchDn = '';
    protected $_dnAttributes = array('uid');
    protected $_filter = array('all' => 'objectClass=*'); // defined filter(s)
    protected $_objectClasses = array('account', 'posixAccount'); // allow all object classes

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Person CLdapRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function getNextUid()
    {
        /* @var PamComponent $comp */
        $comp           = Yii::app()->getComponent('ldap');
        $systemAccounts = SystemAccounts::model()->findByDn($comp->nextUidAccount);
        if ($systemAccounts !== null) {
            $nextUid                   = $systemAccounts->uid + 1;
            $nextUidRecord             = new SystemAccounts();
            $nextUidRecord->attributes = $systemAccounts->getAttributes();
            $nextUidRecord->uid        = $nextUid;
            if ($systemAccounts->delete() && $nextUidRecord->save()) {
                return $nextUid;
            }
        }
        return 0;
    }

    protected function beforeSave()
    {


        if ($this->getIsNewRecord()) {
            if ($this->uidNumber = $this->getNextUid()) {
                $group     = new Group;
                $group->cn = $this->uid;
                if ($group->save()) {
                    $this->gidNumber = $group->gidNumber;
                    return true;
                }
                return false;
            }
            return false;
        }
        return parent::beforeSave();
    }
}