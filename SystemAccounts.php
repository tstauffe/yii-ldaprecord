<?php

/**
 * Class SystemAccounts
 * @property array $_filter
 * @property array $_objectClasses
 * @property $uid
 *
 * @return CLdapRecord
 */
class SystemAccounts extends CLdapRecord
{
    protected $_overwrite = true;
    protected $_branch = 'ou=SystemAccounts';
    protected $_branchDn = '';
    protected $_dnAttributes = array('cn');
    protected $_filter = array('all' => 'objectClass=*'); // defined filter(s)
    protected $_objectClasses = array('organizationalPerson', 'person','uidObject'); // allow all object classes
    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return SystemAccounts CLdapRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

}