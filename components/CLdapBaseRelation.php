<?php
/**
 * CLdapBaseRelation is the base class for all ldap relations.
 *
 * @author Christian Wittkowski <wittkowski@devroom.de>
 * @version $Id: $
 * @package ext.ldaprecord
 * @since 0.4
 */
abstract class CLdapBaseRelation extends CComponent
{
    public $name; // name of the relation
    public $attribute; // own attribute
    public $className; // foreign ldap record class
    public $foreignAttribute; // foreign attribute
    public $options = array();

    /**
     * Constructor.
     * @param string name of the relation
     * @param string name of the own attribute
     * @param string name of the related ldap record class
     * @param string name of the foreign attriubte for this relation
     * @param array additional options (name=>value). The keys must be the property names of this class.
     */
    public function __construct($name, $attribute, $className, $foreignAttribute, $options = array())
    {
        $this->name             = $name;
        $this->attribute        = $attribute;
        $this->className        = $className;
        $this->foreignAttribute = $foreignAttribute;
        $this->options          = $options;
    }

    abstract public function createRelationalRecord($model);
}