<?php
/**
 * CLdapHasOne represents the parameters specifying a HAS_ONE relation.
 *
 * @author Christian Wittkowski <wittkowski@devroom.de>
 * @version $Id: $
 * @package ext.ldaprecord
 * @since 0.4
 */
class CLdapHasOne extends CLdapBaseRelation
{
    public function createRelationalRecord($model)
    {
        if ('dn' == $this->attribute) {
            $template = $this->foreignAttribute;
            eval("\$branchDn = $template;");
            //echo "branchDn: $branchDn" . '<br/>';
            $criteria             = array();
            $criteria['branchDn'] = $branchDn;
            $criteria['attr']     = array();
        } else {
            $attr     = $this->attribute;
            $criteria = array('attr' => array($this->foreignAttribute => $model->$attr));
        }
        foreach ($this->options as $key => $value) {
            $criteria['attr'][$key] = $value;
        }
        //echo 'Criteria: <pre>' . print_r($criteria, true) . '</pre>';
        $results = CLdapRecord::model($this->className)->findAll($criteria);
        //echo 'Result: <pre>' . print_r($results, true) . '</pre>';
        if (0 == count($results)) {
            return null;
        } else {
            if (1 < count($results)) {
                throw new CLdapException('Relation ' . __CLASS__ . ' between ');
            }
        }
        return $results[0];
    }
}