<?php
/**
 * CLdapRecordMetaData represents the meta-data for aa Ldap Record class.
 *
 * @author Christian Wittkowski <wittkowski@devroom.de>
 * @version $Id: $
 * @package ext.ldaprecord
 * @since 0.4
 */
class CLdapRecordMetaData
{
    public $relations = array();

    /**
     * @var array list of relations
     */
    private $_model;

    /**
     * Constructor.
     * @param CLdapRecord $model the model instance
     */
    public function __construct($model)
    {
        $this->_model = $model;

        foreach ($model->relations() as $name => $config) {
            $this->addRelation($name, $config);
        }
    }

    /**
     * Adds a relation.
     *
     * $config is an array with three elements:
     * relation type, own attribute, the related ldap record class and the foreign attribute.
     *
     * @throws CLdapException
     * @param string $name Name of the relation.
     * @param array $config Relation parameters.
     * @return void
     */
    public function addRelation($name, $config)
    {
        if (isset($config[0], $config[1], $config[2], $config[3])) {
            if (isset($config[4])) {
                $this->relations[$name] = new $config[0]($name, $config[1], $config[2], $config[3], $config[4]);
            } else {
                $this->relations[$name] = new $config[0]($name, $config[1], $config[2], $config[3]);
            }
        } else {
            throw new CLdapException(Yii::t(
                    'LdapRecord.record',
                    'Ldap record "{class}" has an invalid configuration for relation "{relation}". It must specify the relation type, the related active record class and the foreign key.',
                    array('{class}' => get_class($this->_model), '{relation}' => $name)
                ));
        }
    }
}